import json
import os
import csv
from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
from mysql import connector
# import mysql.connector
import datetime 

__UPLOADS__ = "file_uploads/single_file/"


conn = connector.connect(host="localhost",
                         user="root",
                         passwd="positive@1234",
                         db="tornado")


class User_API_Handler(RequestHandler):

    def get(self):
        cursor = conn.cursor()
        cursor.execute("Select * from user")
        result = cursor.fetchall()  # get any/all rows
        user_val = []
        for res in result:
            dat = {
                'id': res[0],
                'name': res[1],
                'city': str(res[2]).replace('\r\n', '')
            }
            user_val.append(dat)

        self.write({'User_info': user_val})

    def post(self):
        data = json.loads(self.request.body)
        val = {
            'name': data['name'],
            'city': data['city']
        }
        cursor = conn.cursor()
        # sql = "INSERT INTO user (name,city) VALUES (%s, %s)"
        # value = (val['name'],val['city'])
        # cursor.execute(sql,value)
        cursor.execute("INSERT INTO user (name,city) VALUES (%s, %s) ", (val['name'], val['city']))
        conn.commit()
        self.write({'User_info': val})


class Update_API_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        # sql = "Select * from user where id = %s "
        # args = [id]
        cursor.execute("Select * from user where id = %s ", (id,))
        result = cursor.fetchone()
        self.write({'User_info':
                    {
                        'id': result[0],
                        'name': result[1],
                        'city': str(result[2]).replace('\r\n', '')
                    }})

    def post(self, id):
        data = json.loads(self.request.body)
        val = {
            'name': data['name'],
            'city': data['city']
        }
        print(val)
        cursor = conn.cursor()
        # sql = "UPDATE user set name = %s, city = %s where id = %s "
        # value = (val['name'],val['city'],id)
        # cursor.execute(sql,value)
        cursor.execute("UPDATE user set name = %s, city = %s where id = %s ",
                       (val['name'], val['city'], id,))
        conn.commit()
        self.write({'User_info': val})


class Delete_API_Handler(RequestHandler):

    def delete(self, id):
        cursor = conn.cursor()
        # sql = "Select * from user where id = %s "
        # args = [id]
        cursor.execute("DELETE from user where id = %s ", (id,))
        conn.commit()
        self.write({'message': 'User Deleted Successfully'})


class User_Add_Handler(RequestHandler):
    def get(self):
        self.render("user_add.html")

    def post(self):

        path = "file_uploads/single_file/"

        # file1 = self.request.files['myfile'][0]
        # original_fname = file1['filename']
        # output_file = open(path + original_fname, 'wb')
        # output_file.write(file1['body'])

        name = self.get_argument("name")
        city = self.get_argument("city")
        files = self.request.files['myfile'][0]
        if files:
            fname = files['filename']
            extension = os.path.splitext(fname)[1]
            image_list = ['.png', '.jpg', '.jpeg', '.gif']
            file_list = ['.pdf', '.doc', '.docx', '.xlsx', '.txt']
            file_paths = path + fname

            if extension in image_list:
                # file_paths = path + fname
                # img = Image.open(StringIO(files['body']))
                # print(img)
                # img.save("../img/", img.format)
                fh = open(file_paths, 'w')
                fh.write(str(files['body']))
            elif extension in file_list:
                # file_paths = path + fname
                fh = open(file_paths, 'w')
                fh.write(str(files['body']))
            elif (extension.endswith('.csv')):
                print('csv files')
                # fh = open(file_paths, 'w')
                # fh.write(str(files['body']))
                # csv_data = csv.reader(open(str(file_paths['body'])))
                # print(csv_data)
                # for row in csv_data:
                #     print(row)
                with open(files['body'], 'r') as file_read:
                    reader = csv.reader(file_read)
                    for row in reader:
                        with open(fname, 'w') as file_write:
                            writer = csv.writer(file_write)
                            writer.writerows(row)
        cursor = conn.cursor()
        # upload_path = os.path.join('file_uploads/single_file/' + fname)
        # print(upload_path)
        cursor.execute("INSERT INTO user (name,city,file,file_path) VALUES (%s,%s, %s, %s) ",
                       (name, city, fname, file_paths, ))
        conn.commit()
        self.redirect("/user_list")


class User_List_Handler(RequestHandler):

    def get(self):
        cursor = conn.cursor()
        sql = "SELECT * FROM user "
        cursor.execute(sql)
        row = cursor.fetchall()
        user_val = []
        # num =1
        for rw in row:
            dat = {
                # 'id':num,
                'id': rw[0],
                'name': rw[1],
                'city': str(rw[2]).replace('\r\n', ''),
                'file': rw[3]
            }
            user_val.append(dat)
            # num +=1
        # print(data)
        # print(user_val)
        self.render("user_list.html", data=user_val)

    def post(self):
        pass


class User_Edit_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        cursor.execute("Select * from user where id = %s ", (id,))
        result = cursor.fetchone()
        data = {
            'id': result[0],
            'name': result[1],
            'city': str(result[2]).replace('\r\n', '')
        }
        self.render("user_edit.html", data=data)

    def post(self, id):
        name = self.get_argument("name")
        city = self.get_argument("city")
        print(name, city)
        cursor = conn.cursor()
        # sql = "UPDATE user set name = %s, city = %s where id = %s "
        # value = (val['name'],val['city'],id)
        # cursor.execute(sql,value)
        cursor.execute(
            "UPDATE user set name = %s, city = %s where id = %s ", (name, city, id,))
        conn.commit()
        self.redirect("/user_list")


class User_View_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        cursor.execute("Select * from user where id = %s ", (id,))
        result = cursor.fetchone()
        print(result)
        data = {
            'id': result[0],
            'name': result[1],
            'city': str(result[2]).replace('\r\n', '')
        }
        self.render("user_view.html", data=data)


class User_Delete_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        # sql = "Select * from user where id = %s "
        # args = [id]
        cursor.execute("DELETE from user where id = %s ", (id,))
        conn.commit()
        # self.set_status(200)
        self.write('User Deleted Successfully')
        self.redirect("/user_list")


class File_Handler(RequestHandler):

    def post(self):
        if self.request.files is not None:
            single_uploadFile = self.request.files['single_file'][0]
            single_file_name = single_uploadFile['filename']
            single_output_file = open(
                "file_uploads/single_file/" + single_file_name, 'wb')
            bulk_uploadFile = self.request.files['bulk_file']
        for bulk_file in bulk_uploadFile:
            bulk_file_name = bulk_file['filename']
            bulk_output_file = open(
                "file_uploads/bulk_file/" + bulk_file_name, 'wb')
        self.redirect("/user_list")


class File_Download_Handler(RequestHandler):

    def get(self, id):
        cursor = conn.cursor()
        cursor.execute("SELECT * from user where id = %s ", (id,))
        file_name = cursor.fetchone()
        data = {
            # 'id':file_name[0],
            # 'name':file_name[1],
            # 'city':str(file_name[2]).replace('\r\n', ''),
            'file': file_name[3]
        }
        file_name = data['file']
        # file_dir = os.path.join('file_uploads/single_file')
        file_dir = os.path.join('file_uploads/single_file')

        filepath = "%s/%s" % (file_dir, file_name)
        buf_size = 4096
        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition',
                        'attachment; filename=' + file_name)
        output_file = open(filepath, 'rb')
        self.write(output_file.read())


class User_Login_Handler(RequestHandler):
    def get(self):
        self.render("login.html")


class CheckBox_Handler(RequestHandler):
    def get(self):
        self.render("checkbox.html")


class Ajax_Handler(RequestHandler):
    def post(self):
        print('Ajax calling')
        dat = self.get_argument("data")
        self.write(dat)
        # self.redirect("/user_list")

    # def post(self, *args):
    #     """Example handle ajax post"""
    #     dic = escape.json_decode(self.request.body)
    #     print(dic)
    #     # useful code goes here
    #     self.write(json.dumps({'status': 'ok', 'sent': dic}))
    #     self.finish()


class User_Profile_Handler(RequestHandler):
    def post(self):
        # data = self.request.body
        username = self.get_argument("uname")
        password = self.get_argument("pwd")
        data = {
            'uname': username,
            'pwd': password,
        }
        self.render("profile.html", data=data)


class Dynamic_Form_Handler(RequestHandler):
    def get(self):
        self.render("dynamic_form.html")

    def post(self):
        # dynamic_data = []
        name = self.get_arguments('name[]')
        age = self.get_arguments('age[]')
        city = self.get_arguments('city[]')
        names = (','.join(name))
        ages = (','.join(age))
        cities = (','.join(city))
        print(names)
        print(ages)
        print(cities)
        i = 0
        cursor = conn.cursor()
        # for nam in name:
        #     dat = {
        #     'name':nam,
        #     'age':age[i],
        #     'city':city[i]
        #     }
        #     # print(dat['name'])
        #     cursor.execute("INSERT INTO dynamic_users (name,age, city) VALUES (%s,%s, %s) ", (dat['name'], dat['age'],dat['city']))
        #     conn.commit()
        #     # dynamic_data.append(dat)
        #     i =i+1
        cursor.execute(
            "INSERT INTO dynamic_users (name,age,city) VALUES (%s,%s, %s) ", (names, ages, cities))
        conn.commit()
        # print(dynamic_data)
        # self.render("dynamic_datatable.html", data=dynamic_data)
        self.redirect("/dynamic_user_list")


class Dynamic_User_List_Handler(RequestHandler):

    def get(self):
        cursor = conn.cursor()
        sql = "SELECT * FROM dynamic_users "
        cursor.execute(sql)
        row = cursor.fetchall()
        # print(row)
        dynamic_data = []
        # num =1
        for rw in row:
            dat = {
                # 'id':num,
                'id': rw[0],
                'name': rw[1],
                'age': str(rw[2]).replace('\r\n', ''),
                'city': rw[3]
            }
            # print(dat)
            dynamic_data.append(dat)
        #     # num +=1
        self.render("dynamic_datatable.html", data=dynamic_data)


class Dynamic_User_Edit_Handler(RequestHandler):

    def get(self, id=None):
        # print(id)
        cursor = conn.cursor()
        # sql = ("SELECT * FROM dynamic_users where id = %s", (id,))
        cursor.execute("SELECT * FROM dynamic_users where id = %s", (id,))
        row = cursor.fetchone()
        name = row[1].split(',')
        age = row[2].split(',')
        city = row[3].split(',')
        dynamic_data = []
        i = 0
        for nam in name:
            data = {
                'id': row[0],
                'name': nam,
                'ages': age[i],
                'cities': city[i],
            }
            dynamic_data.append(data)
            i += 1

        print(dynamic_data)
        # self.write('dynamic_data is ' + '\n' + str(dynamic_data) )
        self.render('dynamic_user_edit.html', data=dynamic_data)

    def post(self, id):
        print(id)
        name = self.get_arguments("name[]")
        age = self.get_arguments("age[]")
        city = self.get_arguments("city[]")
        names = (','.join(name))
        ages = (','.join(age))
        cities = (','.join(city))
        print(names)
        print(ages)
        print(cities)
        cursor = conn.cursor()
        cursor.execute(
            "UPDATE dynamic_users set name = %s, age = %s, city = %s where id = %s ", (names, ages, cities, id,))
        conn.commit()
        self.redirect("/dynamic_user_list")


class Dynamic_User_View_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        cursor.execute("Select * from dynamic_users where id = %s ", (id,))
        row = cursor.fetchone()
        name = row[1].split(',')
        age = row[2].split(',')
        city = row[3].split(',')
        dynamic_data = []
        i = 0
        for nam in name:
            data = {
                'id': row[0],
                'name': nam,
                'ages': age[i],
                'cities': city[i],
            }
            dynamic_data.append(data)
            i += 1

        # print(dynamic_data)
        # self.write('dynamic_data is ' + '\n' + str(dynamic_data) )
        self.render('dynamic_user_view.html', data=dynamic_data)


class Dynamic_User_Delete_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        # sql = "Select * from user where id = %s "
        # args = [id]
        cursor.execute("DELETE from dynamic_users where id = %s ", (id,))
        conn.commit()
        # self.set_status(200)
        self.write('Dynamic User Deleted Successfully')
        self.redirect("/dynamic_user_list")


class Product_Add_Handler(RequestHandler):
    def get(self):
        self.render("product_add.html")


class User_CheckBox_Ajax_Edit_Handler(RequestHandler):
    def get(self):
        print('checkbox_user')
        checkbox_data = self.get_argument('data')
        list = json.loads(checkbox_data)
        # print(type(checkbox_data))
        # print(type(json.loads(checkbox_data)))
        checkbox_arr = []
        for dat in list:
            data = {
                'name': dat['name'],
                'city': dat['city'],
            }
            checkbox_arr.append(data)
            # print(data)
            print(checkbox_arr)
        self.render('checkbox_edit.html', data=checkbox_arr)
        # self.redirect("/checkbox_edit")


class User_CheckBox_Ajax_Delete_Handler(RequestHandler):
    def get(self):
        checkbox_data = self.get_argument('data')
        list = json.loads(checkbox_data)
        # print(type(checkbox_data))
        # print(type(json.loads(checkbox_data)))
        checkbox_arr = []
        for dat in list:
            data = {
                'id': dat['id']
            }

            print(data['id'])
        cursor = conn.cursor()
        # sql = "Select * from user where id = %s "
        # args = [id]
        cursor.execute("DELETE from user where id = %s ", (data['id'],))
        conn.commit()
        # self.set_status(200)
        self.finish(' User Deleted Successfully')
        self.redirect("/user_list")


class CheckBox_Edit_Handler(RequestHandler):
    def post(self):
        cursor = conn.cursor()
        id = self.get_arguments('checkbox_id[]')
        name = self.get_arguments('name[]')
        city = self.get_arguments('city[]')
        # print(id)
        # print(name)
        # print(city)
        i = 0
        for id in id:
            data = {
                'id': id,
                'name': name[i],
                'city': city[i],
            }
            i += 1
            # print(data)
            cursor.execute("UPDATE user set name = %s, city = %s where id = %s ",
                           (data['name'], data['city'], data['id'],))
            conn.commit()
        self. redirect('/user_list')


class CheckBox_Delete_Handler(RequestHandler):
    def post(self):
        cursor = conn.cursor()
        id = self.get_arguments('checkbox_id[]')
        for id in id:
            cursor.execute("DELETE from user where id = %s ", (id,))
            conn.commit()
        self.redirect("/user_list")


class Product_Add_Ajax_Handler(RequestHandler):
    def post(self):
        cursor = conn.cursor()
        prod_val = self.get_argument('data')
        product_details = json.loads(prod_val)
        current_date = datetime.date.today()
        update_date = datetime.date.today()
        # print(current_date,update_date)
        # print('current_date',current_date.strftime('%Y-%m-%d'))
        # print('update_date',update_date.strftime('%Y-%m-%d'))
        # sql = "select * from PERSON where F_Name = '%s' or L_Name = '%s' or Age = '%s' or Gender = '%s' " %(self.fname, self.lname, self.age, self.gender)

        cursor.execute("SELECT t1.id, t1.name, t1.price, t1.quantity,t1.created_by, t2.gstpercentage, t2.grandgst, t2.subtotal, t2.grandtotal, t2.created_by FROM products as t1 inner join product_detail as t2 on t1.id = t2.orderid where t1.name = %s and t2.gstpercentage = %s ",(product_details['prod_name'], product_details['gst_percentage'],))
        row = cursor.fetchone()
        print(row)
        grandgst = round(int(product_details['gst_percentage']) * int(product_details['quantity']) * int(product_details['price']) / 100)
        subtotal = int(product_details['price']) * int(product_details['quantity'])
        grandtotal = grandgst + subtotal
        
        if (row is None):
            cursor.execute("INSERT INTO products (name,price,quantity,created_by) VALUES (%s,%s, %s,%s) ", (product_details['prod_name'], int(product_details['price']), int(product_details['quantity']),current_date))
            last_id = cursor.lastrowid
            # , gstpercentage, grandgst, subtotal, grandtotal) VALUES (%s,%s, %s,%s,%s,%s,%s) ", (product_details['prod_name'], int(product_details['price']), int(product_details['quantity']),int(product_details['gst_percentage']),grandgst,subtotal,grandtotal))
            cursor.execute("INSERT INTO product_detail (orderid,gstpercentage, grandgst, subtotal, grandtotal, created_by) VALUES (%s,%s, %s,%s,%s,%s) ",(last_id, int(product_details['gst_percentage']), grandgst, subtotal, grandtotal, current_date))
            conn.commit()
        elif (row[4] == current_date):
            print('current_date',current_date, row[4])
            if (product_details['prod_name'] == row[1]  and int(product_details['gst_percentage']) == row[5]):
                print('gst is', product_details['gst_percentage'])
                final_qty = row[3] + 1
                final_price = row[2] * final_qty
                final_gst = round(final_price * int(product_details['gst_percentage']) / 100)
                sub_total = final_price
                total = sub_total + final_gst
                               
                cursor.execute(" UPDATE products, product_detail SET products.name = %s, products.price = %s, products.quantity = %s, products.updated_by = %d, product_detail.gstpercentage = %s, product_detail.grandgst = %s,product_detail.subtotal = %s, product_detail.grandtotal = %s, product_detail.updated_by = %d WHERE products.id = product_detail.orderid and products.name = %s and product_detail.gstpercentage = %s and products.created_by = %d and product_detail.created_by = %d ", (product_details['prod_name'], final_price, final_qty,update_date, int(product_details['gst_percentage']), final_gst, sub_total, total,update_date, product_details['prod_name'], product_details['gst_percentage'], current_date, current_date))

                # cursor.execute("UPDATE products SET t1.name = %s, t1.price = %s, t1.quantity = %s,t2.gstpercentage = %s,t2.grandgst = %s,t2.subtotal = %s,t2.grandtotal = %s FROM products as t1, product_detail as t2 WHERE t1.name = %s and t2.gstpercentage = %s ", (product_details['prod_name'], final_price, final_qty, int(product_details['gst_percentage']), final_gst, sub_total, total, product_details['prod_name'], product_details['gst_percentage']))
                conn.commit()
            else:
                cursor.execute("INSERT INTO products (name,price,quantity,created_by) VALUES (%s,%s, %s,%s) ", (product_details['prod_name'], int(product_details['price']), int(product_details['quantity']),current_date))
                last_id = cursor.lastrowid
                cursor.execute("INSERT INTO product_detail (orderid,gstpercentage, grandgst, subtotal, grandtotal, created_by) VALUES (%s,%s, %s,%s,%s,%s) ",(last_id, int(product_details['gst_percentage']), grandgst, subtotal, grandtotal, current_date))
                conn.commit()
       

            

        # if (row is None):
        #     last_id = cursor.execute("INSERT INTO products (name,price,quantity) VALUES (%s,%s, %s) ", (product_details['prod_name'], int(product_details['price']), int(product_details['quantity'])))
        #     print(last_id)
        #     # , gstpercentage, grandgst, subtotal, grandtotal) VALUES (%s,%s, %s,%s,%s,%s,%s) ", (product_details['prod_name'], int(product_details['price']), int(product_details['quantity']),int(product_details['gst_percentage']),grandgst,subtotal,grandtotal))
        #     conn.commit()
        # elif(product_details['prod_name'] == row[1]):
        #     print('product name is',product_details['prod_name'] )
        #     if(int(product_details['gst_percentage']) == row[4] ):
        #         print('gst is',product_details['gst_percentage'] )
        #         final_qty = row[3] + 1
        #         final_price = row[2] * final_qty
        #         final_gst = round(final_price * int(product_details['gst_percentage']) / 100 )
        #         sub_total = final_price
        #         total = sub_total + final_gst
        #         cursor.execute("UPDATE products set name = %s, price = %s, quantity = %s, gstpercentage = %s,grandgst = %s,subtotal = %s,grandtotal = %s where name = %s and gstpercentage = %s ", (product_details['prod_name'], final_price,final_qty,int(product_details['gst_percentage']),final_gst,sub_total,total, product_details['prod_name'], product_details['gst_percentage']))
        #         conn.commit()
        #     else:
        #         cursor.execute("INSERT INTO products (name,price,quantity, gstpercentage, grandgst, subtotal, grandtotal) VALUES (%s,%s, %s,%s,%s,%s,%s) ", (product_details['prod_name'], int(product_details['price']), int(product_details['quantity']),int(product_details['gst_percentage']),grandgst,subtotal,grandtotal))
        #         conn.commit()

        # print(product_details['prod_name'])
        # id = self.get_arguments('checkbox_id[]')
        # for id in id:
        #     cursor.execute("DELETE from user where id = %s ", (id,))
        #     conn.commit()
        # self.redirect("/user_list")


class Product_List_Handler(RequestHandler):

    def get(self):
        cursor = conn.cursor()
        # sql = "SELECT * FROM products"
        sql = " SELECT t1.id, t1.name, t1.price, t1.quantity, t2.gstpercentage, t2.grandgst, t2.subtotal, t2.grandtotal from products as t1 inner join product_detail as t2 on t1.id = t2.orderid where t1.status =1 and t2.status= 1"
        # print(sql)
        cursor.execute(sql)
        row = cursor.fetchall()
        prod_data = []
        # num =1
        for rw in row:
            dat = {
                # 'id':num,
                'id': rw[0],
                'prod_name': rw[1],
                'price': rw[2],
                'qty': rw[3],
                'gst': rw[4],
                'final_gst': rw[5],
                'sub_total': rw[6],
                'total': rw[7],
            }
            # print(dat)
            prod_data.append(dat)
            # num +=1
        self.render("product_list.html", data=prod_data)

class Product_Edit_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        cursor.execute("SELECT t1.id, t1.name, t1.price, t1.quantity, t2.gstpercentage, t2.grandgst, t2.subtotal, t2.grandtotal from products as t1 inner join product_detail as t2 on t1.id = t2.orderid where t1.id = %s and t2.orderid = %s and t1.status=1 and t2.status =1", (id,id))
        result = cursor.fetchone()
        # print(result)
        data = {
            'id': result[0],
            'prod_name': result[1],
            'price':result[2],
            'quantity':result[3],
            'gst':result[4],
            'final_gst':result[5],
            'sub_total':result[6],
            'total':result[7],
        }
        # print(data)
        self.render("product_edit.html", data=data)

    def post(self, id):
        name = self.get_argument("name")
        city = self.get_argument("city")
        print(name, city)
        cursor = conn.cursor()
        # sql = "UPDATE user set name = %s, city = %s where id = %s "
        # value = (val['name'],val['city'],id)
        # cursor.execute(sql,value)
        cursor.execute(
            "UPDATE user set name = %s, city = %s where id = %s ", (name, city, id,))
        conn.commit()
        self.redirect("/user_list")


class Product_View_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        cursor.execute("SELECT t1.id, t1.name, t1.price, t1.quantity, t2.gstpercentage, t2.grandgst, t2.subtotal, t2.grandtotal from products as t1 inner join product_detail as t2 on t1.id = t2.orderid where t1.id = %s and t2.orderid = %s ", (id,id))
        result = cursor.fetchone()
        print(result)
        data = {
            'id': result[0],
            'prod_name': result[1],
            'price':result[2],
            'quantity':result[3],
            'gst':result[4],
            'final_gst':result[5],
            'sub_total':result[6],
            'total':result[7],
        }
        self.render("product_view.html", data=data)

class Product_Edit_Add_Handler(RequestHandler):
    def get(self):
        id = self.get_argument('prod_id')
        self.render("product_edit_add.html",id=id)

class Product_Edit_Update_Handler(RequestHandler):
    def post(self):
        cursor = conn.cursor()
        id= self.get_argument('prod_id')
        prod_name= self.get_argument('product')
        gst_percentage= self.get_argument('gst_percentage')
        price= self.get_argument('price')
        quantity= self.get_argument('quantity')
        sub_total= self.get_argument('sub_total')
        final_gst= self.get_argument('final_gst')
        total= self.get_argument('total')
        print(id)
        cursor.execute(" UPDATE products, product_detail SET products.name = %s, products.price = %s, products.quantity = %s,  product_detail.gstpercentage = %s, product_detail.grandgst = %s,product_detail.subtotal = %s, product_detail.grandtotal = %s WHERE products.id = product_detail.orderid and products.id=%s  and product_detail.orderid =%s ", (prod_name,price,quantity, gst_percentage,final_gst,sub_total,total,id,id))
        conn.commit()
        cursor.execute("SELECT t1.id, t1.name, t1.price, t1.quantity, t2.gstpercentage, t2.grandgst, t2.subtotal, t2.grandtotal from products as t1 inner join product_detail as t2 on t1.id = t2.orderid where t1.id = %s and t2.orderid = %s ", (id,id))
        result = cursor.fetchone()
        data = {
            'id': result[0],
            'prod_name': result[1],
            'price':result[2],
            'quantity':result[3],
            'gst':result[4],
            'final_gst':result[5],
            'sub_total':result[6],
            'total':result[7],
        }
        self.render("product_edit.html", data=data)
        
class Product_Edit_Delete_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        del_id = self.get_argument('id')
        print(del_id)
        cursor.execute("SELECT t1.id, t1.name, t1.price, t1.quantity, t2.gstpercentage, t2.grandgst, t2.subtotal, t2.grandtotal from products as t1 inner join product_detail as t2 on t1.id = t2.orderid where t1.id = %s and t2.orderid = %s ", (del_id,del_id))
        result = cursor.fetchone()
        data = {
            'id': result[0],
            'prod_name': result[1],
            'price':result[2],
            'quantity':result[3],
            'gst':result[4],
            'final_gst':result[5],
            'sub_total':result[6],
            'total':result[7],
        }
        cursor.execute("SELECT t1.id, t1.name, t1.price, t1.quantity,t1.status, t2.gstpercentage, t2.grandgst, t2.subtotal, t2.grandtotal,t2.status from products as t1 inner join product_detail as t2 on t1.id = t2.orderid where t1.id = %s and t2.orderid = %s ", (del_id,del_id))
        result = cursor.fetchone()
        cursor.execute("UPDATE products, product_detail SET products.status = 0, product_detail.status = 0 where products.id = %s and product_detail.orderid = %s ", (del_id,del_id))
        # cursor.execute("DELETE products,product_detail FROM products INNER JOIN product_detail ON products.id=product_detail.orderid where products.id = %s and product_detail.orderid = %s ", (id,id))
        conn.commit()
        self.render("product_edit.html", data=data)

class Product_Delete_Handler(RequestHandler):
    def get(self, id):
        cursor = conn.cursor()
        print(id)
        cursor.execute("DELETE products,product_detail FROM products INNER JOIN product_detail ON products.id=product_detail.orderid where products.id = %s and product_detail.orderid = %s ", (id,id))
        conn.commit()
        self.write('User Deleted Successfully')
        self.redirect("/product_list")

class Union_Handler(RequestHandler):
    def get(self):
        cursor = conn.cursor()
        cursor.execute("SELECT id,name,city from user UNION  SELECT id,name,city from employee group by city order by name  ")
        # cursor.execute("SELECT name,city from user UNION ALL SELECT name,city from employee")

        result = cursor.fetchall()
        print(result)
        data = {
            'data':result
        }
        # self.write('data '  +str(result))
        # self.write(json.dumps(data['data'][10]))
        self.write(json.dumps(result, sort_keys=True, indent=4))



settings = {
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
}


def make_app():
    urls = [
        ("/user_api", User_API_Handler),
        ("/update_api/([^/]+)?", Update_API_Handler),
        ("/delete_api/([^/]+)?", Delete_API_Handler),
        ("/user_login", User_Login_Handler),
        ("/user_profile", User_Profile_Handler),
        ("/user_list", User_List_Handler),
        ("/user_add", User_Add_Handler),
        ("/user_edit/([^/]+)?", User_Edit_Handler),
        ("/user_view/([^/]+)?", User_View_Handler),
        ("/user_delete/([^/]+)?", User_Delete_Handler),
        ("/file_upload", File_Handler),
        ("/file_download/([^/]+)?", File_Download_Handler),
        ("/ajax_call", Ajax_Handler),
        ("/checkbox", CheckBox_Handler),
        ("/dynamic_user_add", Dynamic_Form_Handler),
        ("/dynamic_user_list", Dynamic_User_List_Handler),
        # ("/dynamic_user_edit/(.*)", Dynamic_User_Edit_Handler),            # Regex
        ("/dynamic_user_edit/([^/]+)", Dynamic_User_Edit_Handler),
        ("/dynamic_user_view/([^/]+)", Dynamic_User_View_Handler),
        ("/dynamic_user_delete/([^/]+)", Dynamic_User_Delete_Handler),
        ("/product_add", Product_Add_Handler),
        ("/user_checkbox_edit", User_CheckBox_Ajax_Edit_Handler),
        ("/user_checkbox_delete", User_CheckBox_Ajax_Delete_Handler),
        ("/checkbox_edit", CheckBox_Edit_Handler),
        ("/checkbox_delete", CheckBox_Delete_Handler),
        ("/product_add_ajax", Product_Add_Ajax_Handler),
        ("/product_list", Product_List_Handler),
        ("/product_edit/([^/]+)?", Product_Edit_Handler),
        ("/product_view/([^/]+)?", Product_View_Handler),
        ("/product_edit_add", Product_Edit_Add_Handler),
        ("/product_edit_update", Product_Edit_Update_Handler),
        ("/product_edit_delete/([^/]+)?", Product_Edit_Delete_Handler),
        ("/product_delete/([^/]+)?", Product_Delete_Handler),
        ("/union", Union_Handler),

    ]
    return Application(urls, ** settings)


if __name__ == '__main__':
    app = make_app()
    app.listen(8000)
    print("I'm listening port number is 8000")
    IOLoop.instance().start()
